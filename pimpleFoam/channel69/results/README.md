#  Turbulent channel flow scaling results, 69 million cells
 
- scalingdata.m contains manually collected execution times (and clocktime) from the logfiles in ../logfiles. 

- scalingdata.m also plots the results in two figures, number of cores vs speed-up factor and CPUh respectively. 

- .jpg images of results are included for simplicity.

- Currently (first commit) the results for Hebbe are incomplete.

