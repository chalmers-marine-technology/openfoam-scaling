
% clusterName = [Ncores exectime clocktime] 
vera = [32 5199.91 5291;
        64 2457.35 2464;
        124 1229.7 1236;
        256 620.96 623;
        512 330.47 333;
        1024 188.08 198
        2048 116.03 129];
    
tetra = [32 4607.11 4630;
         64 2251.38 2261;
         128 1151.32 1157;
         256 585 590;
         512 303.26 308;
         1024 189.84 197;
         2028 289.23 293];
         
     
 hebbe = [20 13116.6 13616;
          80 2376.98 2387;   
          160 1179.16 1186;
          320 631.45 654
          640 328.95 336
          1280 180.67 185
          2040 153.59 161];
      

% cellsPerCore = 69e3./[20; tetra(:,1)];

%% Plot results:      
close all; 
f = figure; 
% make fullscreen
f.Units= 'normalized';
f.Position = [0 0 1 1];
% Plot speed up: 
plotSpeedUp(vera,'vera','k-x');
hold on; 
plotSpeedUp(tetra,'tetralith','r--o');
plotSpeedUp(hebbe,'hebbe','b-.^');
legend('Location','NorthWest'); 
xlabel('Number of cores, $N_c$');
ylabel('Speed-up')
ax=gca;
%text(ax,840,15.9,'tetralith@2048: 15.9','FontSize',25)
ax.FontSize = 25;
ax.XLim= [0 2050];
ax.YLim = [0 90];
ax.XTick = 32*2.^(0:1:6);
grid on; 

%% Plot time/core directly:

f = figure; 
% make fullscreen
f.Units= 'normalized';
f.Position = [0 0 1 1];
plotCPUh(vera,'vera','k-x');
hold on; 
plotCPUh(tetra,'tetralith','r--o');
plotCPUh(hebbe,'hebbe','b-.^');
ax=gca;
ax.FontSize = 25;
ax.XLim= [0 2050];
ax.XTick = 32*2.^(0:1:6);

%text(ax,840,73.5,'tetralith@2048: 164','FontSize',25);
grid on; 
xlabel('Number of cores, $N_c$')
ylabel('CPU hours, $N_c T_\mathrm{tot}$')
legend('Location','NorthWest'); 

function plotCPUh(d,name,frmt)
    % Ncells = 69e6;
    semilogx(d(:,1),d(:,1).*d(:,2)/3600,frmt,'Linewidth',1.2,'MarkerSize',15,'Displayname',name);    
end

function plotSpeedUp(d,name,frmt)   
   semilogx(d(:,1),d(1,2)./d(:,2),frmt,'Linewidth',1.2,'MarkerSize',15,'Displayname',name);
   hold on; 
   semilogx(d(:,1),d(:,1)/d(1,1),frmt(2:end-1),'Color',[0.8 0.8 0.8],'Linewidth',1.2,'MarkerSize',15,'Displayname',['ideal, ' name]);
end
      