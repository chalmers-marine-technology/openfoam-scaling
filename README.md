#  Parallel Scaling Tests for OpenFOAM

This repository holds data on parallel scaling of OpenFOAM. The ambition is to have test cases for different solvers/algorithms, and run them on the existing SNIC resources.

## Cases

- `pimpleFoam`

  * `channel69`, implicit LES of channel flow.


